﻿namespace NotSteam.Models
{
    public abstract class BaseEntity : BaseModel
    {
        public int Id { get; set; }
    }
}

